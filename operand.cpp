#include "operand.h"
#include <math.h>
#include <QRegExp>

char Operand::separator = '|';

Operand::Operand()
{
    decimalValue = 0;
    numirator = 0;
    denominator = 1;
    //separator = '|';
}

Operand::Operand(long value)
{
    decimalValue = value;
    numirator = value;
    denominator = 1;
    //separator = '|';
}

Operand::Operand(long nom, long denom)
{
    numirator = nom;
    denominator = denom == 0 ? 1 : denom;
    decimalValue = ceil(numirator / denominator);
    //separator = '|';
}

Operand::Operand(QString fraction)
{
    int separatorPosition = fraction.indexOf(separator);
    if (separatorPosition == -1)
    {
        decimalValue = fraction.toInt();
        numirator = decimalValue;
        denominator = 1;
    }
    else
    {
        numirator = fraction.left(separatorPosition).toInt();
        QString tmp = fraction.right(separatorPosition);
        denominator = fraction.right(separatorPosition).toInt();
        decimalValue = ceil(numirator / denominator);
    }
}

long Operand::getNumirator() const
{
    return numirator;
}

void Operand::setNumirator(long value)
{
    numirator = value;
}

long Operand::getDenominator() const
{
    return denominator;
}

void Operand::setDenominator(long value)
{
    denominator = value;
}

char Operand::getSeparator()
{
    return separator;
}

void Operand::setSeparator(char value)
{
    separator = value;
}

long Operand::getDecimalValue() const
{
    return decimalValue;
}

void Operand::setDecimalValue(long value)
{
    decimalValue = value;
}

Operand Operand::operator = (Operand const fraction)
{
    decimalValue = fraction.decimalValue;
    numirator = fraction.numirator;
    denominator = fraction.denominator;
    separator = fraction.separator;

    return *this;
}

Operand Operand::operator + (Operand const fraction)
{
    decimalValue += fraction.decimalValue;

    numirator = numirator*fraction.denominator + denominator*fraction.numirator;
    denominator *= fraction.denominator;
    this->simplify();

    return *this;
}

Operand Operand::operator - (Operand const fraction)
{
    decimalValue -= fraction.decimalValue;

    denominator *= fraction.denominator;
    numirator = numirator*fraction.denominator - fraction.numirator*denominator;
    this->simplify();

    return *this;
}

Operand Operand::operator * (Operand const fraction)
{
    decimalValue *= fraction.decimalValue;

    numirator *= fraction.numirator;
    denominator *= fraction.denominator;
    this->simplify();

    return *this;
}

Operand Operand::operator / (Operand fraction)
{
    decimalValue /= fraction.decimalValue;

    fraction = fraction.Rev();
    numirator *= fraction.numirator;
    denominator *= fraction.denominator;
    this->simplify();

    return *this;
}

Operand Operand::Sqr()
{
    decimalValue *= decimalValue;

    numirator *= numirator;
    denominator *= denominator;

    return *this;
}

Operand Operand::Rev()
{
    return *(new Operand(denominator, numirator));
}

bool Operand::setFraction(QString fraction)
{
    int separatorPosition = fraction.indexOf(separator);
    if (separatorPosition == -1)
    {
        decimalValue = fraction.toInt();
        numirator = decimalValue;
        denominator = 1;
    }
    else
    {
        numirator = fraction.left(separatorPosition).toInt();
        denominator = fraction.right(separatorPosition).toInt();
        decimalValue = ceil(numirator / denominator);
    }
    return true;
}

QString Operand::getFraction(bool isFraction) const
{
    if (isFraction)
    {
        QString tmpNom  = QString::number(numirator);
        QString tmpDenom= QString::number(denominator);
        QString tmpSep  = QString(separator);

        return tmpNom + tmpSep + tmpDenom;
    }
    else return QString::number(decimalValue);
}

Operand Operand::simplify()
{
    long fgcd;
    while ((fgcd = gcd(numirator, denominator)) != 1)
    {
        numirator /= fgcd;
        denominator /= fgcd;
    }

    return *this;
}

long Operand::gcd(long a, long b)
{
    if (b == 0) return a;
    else return gcd(b, a % b);
}
