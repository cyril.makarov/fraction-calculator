#include "calculator.h"
#include "ui_calculator.h"
#include <QClipboard>
#include <QMessageBox>
#include <QRegExp>
#include <QLabel>
#include <math.h>

Calculator::Calculator(QWidget *parent) :
    QWidget(parent)
{
    lastFunction = NULL;

    separator = '|';

    isFraction = false;
    isMemSet  = false;
    isResult = false;

    operand = new Operand();
    result = new Operand();
    memory = new Operand();

    grid = new QGridLayout();
    grid->setGeometry(QRect(100,100,300,300));
    grid->setMargin(70);

    // Menu
    {
        menu = new QMenuBar(this);

        QMenu *menuSettings = menu->addMenu(QString("&Settings"));
        QAction *actionSetDecimal = new QAction(QString("Set decimal"), this);
        connect(actionSetDecimal, SIGNAL(triggered()), this, SLOT(setDecimal()));
        menuSettings->addAction(actionSetDecimal);
        QAction *actionSetFraction = new QAction(QString("Set fraction"), this);
        connect(actionSetFraction, SIGNAL(triggered()), this, SLOT(setFraction()));
        menuSettings->addAction(actionSetFraction);
        QAction *actionSetPipe = new QAction(QString("Set separator \"|\""),this);
        connect(actionSetPipe, SIGNAL(triggered()), this, SLOT(setPipe()));
        menuSettings->addAction(actionSetPipe);
        QAction *actionSetSlash = new QAction(QString("Set separator \"/\""),this);
        connect(actionSetSlash, SIGNAL(triggered()), this, SLOT(setSlash()));
        menuSettings->addAction(actionSetSlash);

        QMenu *menuEdit=menu->addMenu(tr("&Edit"));
        QAction *actionCopy = new QAction(QString("Copy"),this);
        connect(actionCopy, SIGNAL(triggered()), this, SLOT(copy()));
        menuEdit->addAction(actionCopy);
        QAction *actionPaste = new QAction(QString("Paste"),this);
        connect(actionPaste, SIGNAL(triggered()), this, SLOT(paste()));
        menuEdit->addAction(actionPaste);

        QAction *actionAbout = new QAction(QString("About"), this);
        connect(actionAbout, SIGNAL(triggered()), this, SLOT(about()));
        menu->addAction(actionAbout);

        QAction *actionHistory = new QAction(QString("History"), this);
        connect(actionHistory, SIGNAL(triggered()), this, SLOT(showHistory()));
        menu->addAction(actionHistory);

        menu->setGeometry(0,0,300,25);
    }

    // Operands input/output
    displData = new QLineEdit(this);
    displData->setAlignment(Qt::AlignRight);
    displData->setReadOnly(true);
    displData->setText("0");

    // Memory display
    displMemory = new QLineEdit(this);
    displMemory->setAlignment(Qt::AlignRight);
    displMemory->setReadOnly(true);

    // Buttons define
    {
        button1     = new QPushButton(QString("1"),this);
        button2     = new QPushButton(QString("2"),this);
        button3     = new QPushButton(QString("3"),this);
        button4     = new QPushButton(QString("4"),this);
        button5     = new QPushButton(QString("5"),this);
        button6     = new QPushButton(QString("6"),this);
        button7     = new QPushButton(QString("7"),this);
        button8     = new QPushButton(QString("8"),this);
        button9     = new QPushButton(QString("9"),this);
        button0     = new QPushButton(QString("0"),this);
        buttonC     = new QPushButton(QString("C"),this);
        buttonCE    = new QPushButton(QString("CE"),this);
        buttonSqr   = new QPushButton(QString("Sqr(x)"),this);
        buttonRev   = new QPushButton(QString("1/x"),this);
        buttonPlus  = new QPushButton(QString("+"),this);
        buttonMinus = new QPushButton(QString("-"),this);
        buttonMul   = new QPushButton(QString("*"),this);
        buttonDiv   = new QPushButton(QString("/"),this);
        buttonMC    = new QPushButton(QString("MC"),this);
        buttonMS    = new QPushButton(QString("MS"),this);
        buttonMR    = new QPushButton(QString("MR"),this);
        buttonMPlus = new QPushButton(QString("M+"),this);
        buttonResult= new QPushButton(QString("="),this);
        buttonBackspace = new QPushButton(QString("Backspace"),this);
        buttonSeparator = new QPushButton(QString("Separate"), this);
        buttonSeparator->setDisabled(true);
        buttonMC->setDisabled(true);
        buttonMR->setDisabled(true);
        buttonMPlus->setDisabled(true);
    }

    // Placing tooltips
    {
        button1->setToolTip(QString("1"));
        button2->setToolTip(QString("2"));
        button3->setToolTip(QString("3"));
        button4->setToolTip(QString("4"));
        button5->setToolTip(QString("5"));
        button6->setToolTip(QString("6"));
        button7->setToolTip(QString("7"));
        button8->setToolTip(QString("8"));
        button9->setToolTip(QString("9"));
        button0->setToolTip(QString("0"));
        buttonC->setToolTip(QString("Reset calc\nHot key: Esc"));
        buttonCE->setToolTip(QString("Reset operand\nHot key: Del"));
        buttonSqr->setToolTip(QString("Sqr(x)\nHot key: S"));
        buttonRev->setToolTip(QString("1/x\nHot key: R"));
        buttonPlus->setToolTip(QString("+"));
        buttonMinus->setToolTip(QString("-"));
        buttonMul->setToolTip(QString("*"));
        buttonDiv->setToolTip(QString("/"));
        buttonMC->setToolTip(QString("Clear memory"));
        buttonMS->setToolTip(QString("Save to memory"));
        buttonMR->setToolTip(QString("Copy to memory"));
        buttonMPlus->setToolTip(QString("Add to memory"));
        buttonResult->setToolTip(QString("="));
        buttonBackspace->setToolTip(QString("Remove last symbol"));
        buttonSeparator->setToolTip(QString("Insert separator"));
    }

    // Placing ojbects on the grid
    {
        grid->addWidget(displData,0,1,1,3,Qt::AlignTop);
        grid->addWidget(displMemory,0,0,1,1,Qt::AlignTop);
        grid->addWidget(button1,1,1,1,1,Qt::AlignTop);
        grid->addWidget(button2,1,2,1,1,Qt::AlignTop);
        grid->addWidget(button3,1,3,1,1,Qt::AlignTop);
        grid->addWidget(button4,2,1,1,1,Qt::AlignTop);
        grid->addWidget(button5,2,2,1,1,Qt::AlignTop);
        grid->addWidget(button6,2,3,1,1,Qt::AlignTop);
        grid->addWidget(button7,3,1,1,1,Qt::AlignTop);
        grid->addWidget(button8,3,2,1,1,Qt::AlignTop);
        grid->addWidget(button9,3,3,1,1,Qt::AlignTop);
        grid->addWidget(button0,4,2,1,1,Qt::AlignTop);
        grid->addWidget(buttonC,1,5,1,1,Qt::AlignTop);
        grid->addWidget(buttonCE,2,5,1,1,Qt::AlignTop);
        grid->addWidget(buttonMC,1,0,1,1,Qt::AlignTop);
        grid->addWidget(buttonMS,2,0,1,1,Qt::AlignTop);
        grid->addWidget(buttonMR,3,0,1,1,Qt::AlignTop);
        grid->addWidget(buttonMul,3,4,1,1,Qt::AlignTop);
        grid->addWidget(buttonDiv,4,4,1,1,Qt::AlignTop);
        grid->addWidget(buttonRev,3,5,1,1,Qt::AlignTop);
        grid->addWidget(buttonSqr,4,5,1,1,Qt::AlignTop);
        grid->addWidget(buttonPlus,1,4,1,1,Qt::AlignTop);
        grid->addWidget(buttonMinus,2,4,1,1,Qt::AlignTop);
        grid->addWidget(buttonMPlus,4,0,1,1,Qt::AlignTop);
        grid->addWidget(buttonResult,4,3,1,1,Qt::AlignTop);
        grid->addWidget(buttonBackspace,0,4,1,1,Qt::AlignTop);
        grid->addWidget(buttonSeparator,4,1,1,1,Qt::AlignTop);
    }

    // Mapping input buttons with singals and slots
    {
        sigMapperInput = new QSignalMapper(this);
        sigMapperInput->setMapping(button1, QString("1"));
        sigMapperInput->setMapping(button2, QString("2"));
        sigMapperInput->setMapping(button3, QString("3"));
        sigMapperInput->setMapping(button4, QString("4"));
        sigMapperInput->setMapping(button5, QString("5"));
        sigMapperInput->setMapping(button6, QString("6"));
        sigMapperInput->setMapping(button7, QString("7"));
        sigMapperInput->setMapping(button8, QString("8"));
        sigMapperInput->setMapping(button9, QString("9"));
        sigMapperInput->setMapping(button0, QString("0"));
        sigMapperInput->setMapping(buttonSeparator, QString(QString("s")));

        connect(button1,SIGNAL(clicked()),sigMapperInput,SLOT(map()));
        connect(button2,SIGNAL(clicked()),sigMapperInput,SLOT(map()));
        connect(button3,SIGNAL(clicked()),sigMapperInput,SLOT(map()));
        connect(button4,SIGNAL(clicked()),sigMapperInput,SLOT(map()));
        connect(button5,SIGNAL(clicked()),sigMapperInput,SLOT(map()));
        connect(button6,SIGNAL(clicked()),sigMapperInput,SLOT(map()));
        connect(button7,SIGNAL(clicked()),sigMapperInput,SLOT(map()));
        connect(button8,SIGNAL(clicked()),sigMapperInput,SLOT(map()));
        connect(button9,SIGNAL(clicked()),sigMapperInput,SLOT(map()));
        connect(button0,SIGNAL(clicked()),sigMapperInput,SLOT(map()));
        connect(buttonSeparator,SIGNAL(clicked()),sigMapperInput,SLOT(map()));

        connect(sigMapperInput, SIGNAL(mapped(QString)),this, SLOT(processDigit(QString)));
    }

    // Mapping cntl buttons with signals and slots
    {
        sigMapperCntl = new QSignalMapper(this);
        sigMapperCntl->setMapping(buttonC, Qt::Key_Escape);
        sigMapperCntl->setMapping(buttonCE, Qt::Key_Delete);
        sigMapperCntl->setMapping(buttonMul, Qt::Key_Asterisk);
        sigMapperCntl->setMapping(buttonDiv, Qt::Key_Slash);
        sigMapperCntl->setMapping(buttonRev, Qt::Key_R);
        sigMapperCntl->setMapping(buttonSqr, Qt::Key_S);
        sigMapperCntl->setMapping(buttonPlus, Qt::Key_Plus);
        sigMapperCntl->setMapping(buttonMinus, Qt::Key_Minus);
        sigMapperCntl->setMapping(buttonResult, Qt::Key_Enter);
        sigMapperCntl->setMapping(buttonBackspace, Qt::Key_Backspace);

        connect(buttonC,SIGNAL(clicked()),sigMapperCntl,SLOT(map()));
        connect(buttonCE,SIGNAL(clicked()),sigMapperCntl,SLOT(map()));
        connect(buttonMul,SIGNAL(clicked()),sigMapperCntl,SLOT(map()));
        connect(buttonDiv,SIGNAL(clicked()),sigMapperCntl,SLOT(map()));
        connect(buttonRev,SIGNAL(clicked()),sigMapperCntl,SLOT(map()));
        connect(buttonSqr,SIGNAL(clicked()),sigMapperCntl,SLOT(map()));
        connect(buttonPlus,SIGNAL(clicked()),sigMapperCntl,SLOT(map()));
        connect(buttonMinus,SIGNAL(clicked()),sigMapperCntl,SLOT(map()));
        connect(buttonResult,SIGNAL(clicked()),sigMapperCntl,SLOT(map()));
        connect(buttonBackspace,SIGNAL(clicked()),sigMapperCntl,SLOT(map()));

        connect(sigMapperCntl, SIGNAL(mapped(int)),this, SLOT(processCntl(int)));
    }

    // Mapping memory buttons with signals and slots
    {
        sigMapperMem = new QSignalMapper(this);
        sigMapperMem->setMapping(buttonMC, MC);
        sigMapperMem->setMapping(buttonMS, MS);
        sigMapperMem->setMapping(buttonMR, MR);
        sigMapperMem->setMapping(buttonMPlus, MP);

        connect(buttonMC,SIGNAL(clicked()),sigMapperMem,SLOT(map()));
        connect(buttonMS,SIGNAL(clicked()),sigMapperMem,SLOT(map()));
        connect(buttonMR,SIGNAL(clicked()),sigMapperMem,SLOT(map()));
        connect(buttonMPlus,SIGNAL(clicked()),sigMapperMem,SLOT(map()));

        connect(sigMapperMem, SIGNAL(mapped(int)),this, SLOT(processMem(int)));
    }

    this->setLayout(grid);
    this->setFocus();
}

Calculator::~Calculator()
{
    delete result;
    delete operand;
    delete memory;
}

// ========== Operations ==========

void Calculator::operationMul()
{
    *result = *result * *operand;
    this->displayData();
}

void Calculator::operationDiv()
{
    if (operand->getDecimalValue() == 0)
    {
        this->dbgMsg(QString("Division by zero!"));
        return;
    }
    *result = *result / *operand;
    this->displayData();
}

void Calculator::operationPlus()
{
    *result = *result + *operand;
    this->displayData();
}

void Calculator::operationMinus()
{
    *result = *result - *operand;
    this->displayData();
}

void Calculator::operationSqr()
{
    if (!validateInput()) return;
    Operand *tmp = new Operand(displData->text());
    tmp->Sqr();
    displData->setText(tmp->getFraction(isFraction));
    if (isOperationComplete) *result = *tmp;
    else *operand = *tmp;
}

void Calculator::operationRev()
{
    if (!validateInput()) return;
    Operand *tmp = new Operand(displData->text());
    *tmp = tmp->Rev();
    displData->setText(tmp->getFraction(isFraction));
    if (isOperationComplete) *result = *tmp;
    else *operand = *tmp;
}

// ========== Memory operations ==========

void Calculator::memClr()
{
    delete memory;
    memory = new Operand();
    displMemory->setText(memory->getFraction(isFraction));
}

void Calculator::memAdd()
{
    if (!validateInput()) return;
    Operand *tmp = new Operand(displData->text());
    *memory = *memory + *tmp;
    displMemory->setText(memory->getFraction(isFraction));
}

void Calculator::memSave()
{
    buttonMC->setEnabled(true);
    buttonMPlus->setEnabled(true);
    buttonMR->setEnabled(true);

    memory->setFraction(displData->text());
    displMemory->setText(memory->getFraction(isFraction));
}

void Calculator::memCopy()
{
    displData->setText(memory->getFraction(isFraction));
    *operand = *memory;
}

// ========== Misc ==========

void Calculator::displayData()
{
    QString output;
    output = result->getFraction(isFraction);
    displData->setText(output);

    isResult = true;
}

void Calculator::dbgMsg(QString str)
{
    QMessageBox tmp;
    tmp.setText(str);
    tmp.exec();
}

bool Calculator::validateInput()
{
    QRegExp expr("-{,1}[0-9]{1,10}([|/][0-9]{1,10}){,1}");
    if (!expr.exactMatch(displData->text()))
    {
        this->dbgMsg("Something wrong with indput data!");
        return false;
    }
    else return true;
}

// ========== Handlers ==========

void Calculator::keyPressEvent(QKeyEvent *e)
{
    QMessageBox tmp;

    switch (e->key())
    {
    // ==== Processing input buttons ====
    case Qt::Key_0:
    case Qt::Key_1:
    case Qt::Key_2:
    case Qt::Key_3:
    case Qt::Key_4:
    case Qt::Key_5:
    case Qt::Key_6:
    case Qt::Key_7:
    case Qt::Key_8:
    case Qt::Key_9:
        processDigit(QString(e->key()));
        break;
    // ===== Processing cntl buttons ====
    case Qt::Key_Escape:
    case Qt::Key_Delete:
    case Qt::Key_Asterisk:
    case Qt::Key_Slash:
    case Qt::Key_Plus:
    case Qt::Key_S:
    case Qt::Key_R:
    case Qt::Key_Minus:
    case Qt::Key_Enter:
    case Qt::Key_Backspace:
        processCntl(e->key());
        break;
    default:
       // tmp.setText(QString("This button is not a hotkey."));
        //tmp.exec();
        break;
    }
}

bool Calculator::processInput()
{
    if (validateInput())
        if (operand->setFraction(displData->text())) return true;
    return false;
}

void Calculator::processDigit(QString s)
{
    // If operation was completed and new input began
    // before new operation then it's new calculation: reset result and operand
    if (isOperationComplete)
    {
        lastFunction = NULL;
    }
    // If 0 or result in data display and new input began
    // reset result status and data on display
    if (displData->text() == "0" || isResult)
    {
        displData->clear();
        isResult = false;
    }
    if (s == "s") displData->insert(QString(Operand::getSeparator()));
    else displData->insert(s);
}

void Calculator::processCntl(int operationCode)
{
    if (!isResult)
    {
        if (processInput())
        {
            // If first time using control buttons
            // store current operand as a result
            if (lastFunction == NULL)
            {
                *result = *operand;
                isResult = true;
            }
        }
        else
        {
            this->dbgMsg("Something wrong with input!");
        }
    }

    switch(operationCode)
    {
    // CE
    case Qt::Key_Escape:
    {
        // Reset display, input and memory
        displData->setText(QString("0"));
        isMemSet = false;
        isResult = false;
        isOperationComplete = false;

        // Reset result
        delete result;
        result = new Operand(0, 1);

        // Reset operand
        delete operand;
        operand = new Operand(0, 1);

        // Reset last function
        lastFunction = NULL;
        break;
    }

    // C
    case Qt::Key_Delete:
    {
        displData->setText(QString("0"));
        break;
    }

    // *
    case Qt::Key_Asterisk:
    {
        lastFunction = &Calculator::operationMul;
        lastFunctionStr = '*';
        isOperationComplete = false;
        break;
    }

    // /
    case Qt::Key_Slash:
    {
        lastFunction = &Calculator::operationDiv;
        lastFunctionStr = '/';
        isOperationComplete = false;
        break;
    }

    // +
    case Qt::Key_Plus:
    {
        lastFunction = &Calculator::operationPlus;
        lastFunctionStr = '+';
        isOperationComplete = false;
        break;
    }

    // Sqr
    case Qt::Key_S:
    {
        operationSqr();
        break;
    }

    // Rev
    case Qt::Key_R:
    {
        operationRev();
        break;
    }

    // -
    case Qt::Key_Minus:
    {
        lastFunction = &Calculator::operationMinus;
        lastFunctionStr = '-';
        isOperationComplete = false;
        break;
    }

    // =
    case Qt::Key_Enter:
    {
        QString tmp = result->getFraction(isFraction);
        if (lastFunction != NULL) (this->*lastFunction)();
        isOperationComplete = true;
        history.push_back(tmp +
                          QString(lastFunctionStr) +
                          operand->getFraction(isFraction) +
                          QString(" = ") +
                          result->getFraction(isFraction));
        break;
    }

    // Backspace
    case Qt::Key_Backspace:
    {
        displData->backspace();
        break;
    }
    }
}

void Calculator::processMem(int operationCode)
{
    switch(operationCode)
    {
    case MS:
        memSave();
        break;
    case MC:
        memClr();
        break;
    case MR:
        memCopy();
        break;
    case MP:
        memAdd();
        break;
    }
}

// ========== Menu functions ==========

void Calculator::setDecimal()
{
    isFraction = false;
    this->displayData();
    displMemory->setText(memory->getFraction(isFraction));
    buttonSeparator->setDisabled(true);
}

void Calculator::setFraction()
{
    isFraction = true;
    this->displayData();
    displMemory->setText(memory->getFraction(isFraction));
    buttonSeparator->setEnabled(true);
}

void Calculator::setPipe()
{
    Operand::setSeparator('|');
    this->displayData();
    displMemory->setText(memory->getFraction(isFraction));
}

void Calculator::setSlash()
{
    Operand::setSeparator('/');
    this->displayData();
    displMemory->setText(memory->getFraction(isFraction));
}

void Calculator::copy()
{
    QApplication::clipboard()->setText(displData->text());
}

void Calculator::paste()
{
    displData->setText(QApplication::clipboard()->text());
}

void Calculator::about()
{
    // Change this to child window in further
    this->dbgMsg("Калькулятор выполнил Макаров К.Д. группа 3100");
}

void Calculator::showHistory()
{
    QWidget *popup = new QWidget();
    popup->setWindowTitle("History");
    popup->setGeometry(300, 300, 300, 300);

    QGridLayout *popupGrid = new QGridLayout();
    popupGrid->setGeometry(QRect(300,300,300, 300));

    for (int i = 0, j = history.size() - 1; j >= 0 && i < 5; i++, j--)
    {
        QLabel *label = new QLabel(popup);
        label->setText(history[j]);
        popupGrid->addWidget(label,i,0,1,1,Qt::AlignTop);
    }

    popup->setLayout(popupGrid);
    popup->show();
}
