#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QWidget>
#include <QMenuBar>
#include <QPaintEvent>
#include <QPushButton>
#include <QSignalMapper>
#include <QGridLayout>
#include <QLineEdit>
#include <QKeyEvent>
#include <list>
#include "operand.h"

#define MS 1
#define MC 2
#define MR 3
#define MP 4

namespace Ui {
    class Calculator;
}

class Calculator : public QWidget
{
    Q_OBJECT

public:
    explicit Calculator(QWidget *parent = 0);
    ~Calculator();

    QGridLayout *grid;

    QMenuBar *menu;

    QPushButton *button1;
    QPushButton *button2;
    QPushButton *button3;
    QPushButton *button4;
    QPushButton *button5;
    QPushButton *button6;
    QPushButton *button7;
    QPushButton *button8;
    QPushButton *button9;
    QPushButton *button0;
    QPushButton *buttonC;
    QPushButton *buttonCE;
    QPushButton *buttonBackspace;
    QPushButton *buttonSqr;
    QPushButton *buttonRev;
    QPushButton *buttonPlus;
    QPushButton *buttonMinus;
    QPushButton *buttonMul;
    QPushButton *buttonDiv;
    QPushButton *buttonMC;
    QPushButton *buttonMS;
    QPushButton *buttonMR;
    QPushButton *buttonMPlus;
    QPushButton *buttonResult;
    QPushButton *buttonSeparator;

    QSignalMapper *sigMapperInput;
    QSignalMapper *sigMapperCntl;
    QSignalMapper *sigMapperMem;

    QLineEdit *displData;
    QLineEdit *displMemory;

protected:
    void keyPressEvent(QKeyEvent *e);

private:

    vector<QString> history;

    Operand *result;
    Operand *operand;
    Operand *memory;

    char separator;
    char lastFunctionStr;

    bool isMemSet;
    bool isFraction;
    bool isResult;
    bool isOperationComplete;

    void (Calculator::*lastFunction)(void);

    void operationMul();
    void operationDiv();
    void operationPlus();
    void operationMinus();
    void operationSqr();
    void operationRev();

    void memAdd();
    void memClr();
    void memSave();
    void memCopy();

    void displayData();
    void dbgMsg(QString str);
    bool validateInput();

    bool processInput();

public slots:

    void processDigit(QString s);
    void processCntl(int operationCode);
    void processMem(int operationCode);

    void setFraction();
    void setDecimal();
    void setPipe();
    void setSlash();
    void copy();
    void paste();
    void about();
    void showHistory();
};

#endif // CALCULATOR_H
