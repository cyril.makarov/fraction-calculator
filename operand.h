#ifndef MYFRACTION
#define MYFRACTION

#include <QString>
using namespace std;

class Operand
{
public:
    Operand();
    Operand(long value);
    Operand(long numirator, long denominator);
    Operand(QString fraction);

    Operand operator + (Operand const fraction);
    Operand operator - (Operand const fraction);
    Operand operator * (Operand const fraction);
    Operand operator / (Operand fraction);
    Operand operator = (Operand const fraction);

    Operand Sqr();
    Operand Rev();

    QString getFraction(bool isFraction) const;
    bool    setFraction(QString s);

    long getNumirator() const;
    void setNumirator(long value);

    long getDenominator() const;
    void setDenominator(long value);

    static char getSeparator();
    static void setSeparator(char separator);

    long getDecimalValue() const;
    void setDecimalValue(long value);

private:

    long decimalValue;
    long numirator;
    long denominator;

    static char separator;

    Operand simplify();
    long gcd(long a, long b);
};

#endif // MYFRACTION
